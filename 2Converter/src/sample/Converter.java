package sample;

import com.tunyk.currencyconverter.api.CurrencyConverterException;
import com.tunyk.currencyconverter.api.CurrencyNotSupportedException;

/**
 * Created by ABC on 19.02.2015.
 */
public interface Converter {
    public float convert(float v, String in, String out) throws CurrencyConverterException;

    public String[] types();


}
