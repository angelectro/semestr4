package sample;

import com.tunyk.currencyconverter.BankUaCom;
import com.tunyk.currencyconverter.api.Currency;
import com.tunyk.currencyconverter.api.CurrencyConverter;
import com.tunyk.currencyconverter.api.CurrencyConverterException;

/**
 * Created by ABC on 24.02.2015.
 */
public class CyrrencyConvert implements Converter {
    @Override
    public float convert(float v, String in, String out) throws CurrencyConverterException {

            CurrencyConverter converter = new BankUaCom(Currency.fromString(in), Currency.fromString(out));
            System.out.println(converter.convertCurrency(v));
            return converter.convertCurrency(v);
    }

    @Override
    public String[] types() {
        String[] strings = new String[30];
        int i = 0;
        for (Currency currency : Currency.values()) {
            strings[i] = currency.name();
            i++;
        }
        return strings;
    }
}
