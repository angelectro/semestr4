package sample;

import com.tunyk.currencyconverter.api.CurrencyConverterException;

import java.util.HashMap;

/**
 * Created by ABC on 21.02.2015.
 */
public class ConverterManager{
    private static String[] categories= {"Currency","Number system","Temperature"};
    public static String currentIn;
    public static String currentOut;
    private static Converter converterCurrent= null;
    public static String[] selectCategory(String s) throws  Exception
    {
        if(s.equals("Currency")){
           converterCurrent= new CyrrencyConvert();
            return  converterCurrent.types();
        }
        else
        {
              throw new Exception("Select another converter");
        }

    }
    public static float convert(float v) throws CurrencyConverterException
    {
        return converterCurrent.convert(v,currentIn,currentOut);
    }

    public static String[] getCategories() {
        return categories;
    }


}
