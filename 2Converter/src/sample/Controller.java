package sample;

import com.tunyk.currencyconverter.api.CurrencyConverterException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ChoiceBox checkOut;
    public Button buttonConvert;
    public ChoiceBox checkIn;
    public Button buttonInversion;
    public TextField fieldIn;
    public TextField fieldOut;
    public ChoiceBox checkCategory;
    public Label inform;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<String> categ = FXCollections.observableArrayList(ConverterManager.getCategories());
        checkCategory.setItems(categ);
        checkCategory.setTooltip(new Tooltip("Select category"));
        checkCategory.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object o, Object t1) {
                inform.setText("");
                ObservableList<String> categ = null;
                try {
                    categ = FXCollections.observableArrayList(ConverterManager.selectCategory((String) t1));
                } catch (Exception e) {
                    inform.setText(e.getMessage());
                }

                checkIn.setItems(categ);
                checkOut.setItems(categ);
            }
        });
        checkCategory.getSelectionModel().selectFirst();
        checkIn.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object o, Object t1) {
                ConverterManager.currentIn = t1.toString();
            }
        });
        checkOut.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object o, Object t1) {
                ConverterManager.currentOut = t1.toString();
            }
        });

    }


    public void clickConvert(Event event) {
        try {
            fieldOut.setText(Float.toString(ConverterManager.convert(Float.parseFloat(fieldIn.getText()))));
        } catch (CurrencyConverterException e) {
            inform.setText("Error connecting to server!");
            System.err.println(e);
        }
        catch (Exception e)
        {
            inform.setText("uuuups!");
        }

    }

    public void clickInversion(Event event) {
        Object o = checkIn.getValue();
        checkIn.setValue(checkOut.getValue());
        checkOut.setValue(o);
    }
}
