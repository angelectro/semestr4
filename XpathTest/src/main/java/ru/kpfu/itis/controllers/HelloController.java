package ru.kpfu.itis.controllers;

import org.htmlcleaner.XPatherException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.Element;
import ru.kpfu.itis.ElementValidator;
import ru.kpfu.itis.Interfaces.Manager;
import ru.kpfu.itis.ManagerXpath;

import java.io.IOException;

@Controller
@RequestMapping("/")
public class HelloController {
    @Autowired
    Manager xpath;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(new ElementValidator());
    }

    @RequestMapping(method = RequestMethod.GET)
    public String new_element(ModelMap model) {
        model.put("element", new Element());
        return "hello";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String result(RedirectAttributes attributes,
                         @Validated Element element,
                         BindingResult result,
                         ModelMap map) throws Exception {
        if (result.hasErrors())
            return "hello";
        else {
            map.put("element", element);
            String text = null;
            try {
                text=xpath.parse(element);
            } catch (IOException e) {
                result.rejectValue("req","req.failed",new Object[]{},"no connection");
            }
            attributes.addFlashAttribute("message", text);
            return "redirect:" + MvcUriComponentsBuilder.fromMappingName("HC#new_element").build();
        }
    }

}