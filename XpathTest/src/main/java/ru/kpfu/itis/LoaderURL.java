package ru.kpfu.itis;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import ru.kpfu.itis.Interfaces.Loader;

/**
 * Created by ABC on 16.03.2015.
 */
public class LoaderURL implements Loader{

    public InputStream connection(String url_address) throws Exception {
        try {
            URLConnection connection = new URL(url_address).openConnection();
            return connection.getInputStream();
        }
        catch (IOException e) {
            throw new Exception("req.failed");
        }
    }
}
