package ru.kpfu.itis;

/**
 * Created by ABC on 16.03.2015.
 */
public class Element {
    private String url;
    private String req;

    public String getReq() {
        return req;
    }

    public void setReq(String req) {
        this.req = req;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
