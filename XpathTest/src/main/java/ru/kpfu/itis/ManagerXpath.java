package ru.kpfu.itis;

import org.htmlcleaner.XPatherException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import ru.kpfu.itis.Interfaces.*;

/**
 * Created by ABC on 16.03.2015.
 */
public class ManagerXpath implements Manager {
    @Autowired
    private Loader loader;
    @Autowired
    private Parser parser;


    public String parse(Element element) throws Exception {
        InputStream reader = loader.connection(element.getUrl());
        return parser.parse(reader,element.getReq());
    }
}
