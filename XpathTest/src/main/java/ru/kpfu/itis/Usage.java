package ru.kpfu.itis;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.XPatherException;

import java.io.InputStream;

/**
 * Created by ABC on 16.03.2015.
 */
public class Usage {

    public String parse(InputStream reader, String path) throws Exception {
        HtmlCleaner htmlCleaner = new HtmlCleaner();
        TagNode node = htmlCleaner.clean(reader);
        Object[] tags = new Object[0];
        try {
            tags = node.evaluateXPath(path);
        } catch (XPatherException e) {
            throw new Exception("Can't get XML by URL");
        }

        StringBuilder text = new StringBuilder();
        if (tags.length != 0)
            if (tags[0].getClass().getTypeName() == "java.lang.StringBuffer") {
                text.append(tags[0].toString().trim());
            } else {
                TagNode aTag = (TagNode) tags[0];
                text.append(htmlCleaner.getInnerHtml(aTag) + "\n");
            }
        return text.toString();
    }
}
