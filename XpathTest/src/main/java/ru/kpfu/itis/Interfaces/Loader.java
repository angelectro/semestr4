package ru.kpfu.itis.Interfaces;

import java.io.InputStream;

/**
 * Created by ABC on 18.03.2015.
 */
public interface Loader {
        public InputStream connection(String path) throws Exception;
}
