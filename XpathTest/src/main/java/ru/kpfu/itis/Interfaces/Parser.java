package ru.kpfu.itis.Interfaces;

import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by ABC on 18.03.2015.
 */
public interface Parser {
    public String parse(InputStream stream,String path) throws Exception;
}
