package ru.kpfu.itis.Interfaces;

import ru.kpfu.itis.Element;

/**
 * Created by ABC on 18.03.2015.
 */
public interface Manager {
    public String parse(Element element) throws Exception;
}
