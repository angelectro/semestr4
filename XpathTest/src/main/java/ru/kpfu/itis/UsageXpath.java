package ru.kpfu.itis;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.XPatherException;
import ru.kpfu.itis.Interfaces.Parser;

import java.io.InputStream;

/**
 * Created by ABC on 16.03.2015.
 */
public class UsageXpath implements Parser {
    public String parse(InputStream reader, String path) throws Exception {
        HtmlCleaner htmlCleaner = new HtmlCleaner();
        TagNode node = htmlCleaner.clean(reader);
        Object[] tags = null;
        try {
            tags = node.evaluateXPath(path);
        } catch (XPatherException e) {
            throw new Exception("Can't get XML by URL");
        }
        StringBuilder text = new StringBuilder();
        if (tags[0] instanceof TagNode) {
            for (Object tagNode : tags) {
                TagNode node1 = (TagNode) tagNode;
                text.append(htmlCleaner.getInnerHtml(node1)+"\n");
            }
        } else if (tags[0] instanceof StringBuffer) {
            for(Object o:tags) {
                StringBuffer buffer = (StringBuffer) o;
                text.append(o.toString().trim()+"\n");
            }
        }

        return text.toString();
    }

}
