package ru.kpfu.itis;

import org.htmlcleaner.TagNode;
import org.htmlcleaner.XPatherException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by ABC on 16.03.2015.
 */
public class ElementValidator implements Validator {

    @Override
    public boolean supports(Class Class) {
        return Element.class.equals(Class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "url", "url.empty");
        ValidationUtils.rejectIfEmpty(errors,"req","req.empty");
        Element element = (Element) o;
        try {
            new URL(element.getUrl());
            new TagNode("").evaluateXPath(element.getReq());
        } catch (MalformedURLException e) {
             errors.rejectValue("url","url.incorrect");
        } catch (XPatherException e) {
            errors.rejectValue("req","req.incxml");
        }
    }
}
