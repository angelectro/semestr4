package ru.kpfu.bookstore;

import java.net.MalformedURLException;
import java.net.URL;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class AuthorValidator implements Validator {

  @Override
  public boolean supports(Class clazz) {
    return Author.class.equals(clazz);
  }

  @Override
  public void validate(Object obj, Errors e) {
    Author a = (Author) obj;
      if(a.getName().isEmpty())
          e.rejectValue("name","empty", new Object[]{"name"},"empty");
    try {
      new URL(a.getAvatarUrl());
    } catch (MalformedURLException ex) {
      e.rejectValue("avatarUrl", "URL.incorrect", new Object[]{"URL.incorrect"},"incorrect url");
    }
  }
}
