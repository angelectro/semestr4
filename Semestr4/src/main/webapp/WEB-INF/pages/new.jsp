<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ABC
  Date: 01.11.14
  Time: 0:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.js" />"></script>
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<c:url value="/resources/jquery-2.1.1.min.js"/>"></script>
    <title>Новинки</title>
</head>
<body style="background-color: #b0b0b0">
<div class="container">
    <jsp:include page="layouts/navigation.jsp"></jsp:include>
    <div class="col-xs-12 col-sm-9">
        <div style="padding-left: 50px">
          <c:forEach var="film" items="${films}" varStatus="status">
            <div class="panel panel-info">
                <div class="panel-heading"><a href="film?id=${film.id}"><h4>${film.name} (${film.year})</h4> </a>
                </div>
                <div style="padding: 10px;">
                    <div style="width: 20%;margin: 5px; display: inline-block"><img class="img-responsive"
                                                                                    src="${film.img_source}">
                    </div>
                    <div style="display: inline-block;width: 75%;vertical-align: top" class="alert alert-success">
                        "${film.text}"
                    </div>
                </div>

            </div>
          </c:forEach>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">

    </div>
</div>
</body>
</html>
