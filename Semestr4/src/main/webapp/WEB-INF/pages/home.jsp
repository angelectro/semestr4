<%-- Created by IntelliJ IDEA. --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.js" />"></script>
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<c:url value="/resources/jquery-2.1.1.min.js"/>"></script>
    <title>Главная</title>
</head>
<body style="background-color: #b0b0b0">
<div class="container">
    <jsp:include page="layouts/navigation.jsp"></jsp:include>
    <div class="col-xs-12 col-sm-9">
        <div>
            <div id="myCarousel" class="carousel carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                    <c:forEach var="slid" items="${listSlider}" varStatus="status">
                        <div class="item<c:if test="${slid.first}"> active</c:if>">
                            <img src="<c:url value="${slid.img_src}"/>" alt="First slide">

                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>1</h1>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"></a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
    </div>
    <div></div>
</div>
</body>
</html>
