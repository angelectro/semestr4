<%--
  Created by IntelliJ IDEA.
  User: ABC
  Date: 31.10.14
  Time: 15:58
  To change this template use File | Settings | File Templates.
--%>
<%-- Created by IntelliJ IDEA. --%>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%
    InitialContext initContext = new InitialContext();
    DataSource ds = null;
    try {
        ds = (DataSource) initContext.lookup("java:comp/env/jdbc/postgres");
    } catch (NamingException e) {
        e.printStackTrace();
    }
    Connection conn = ds.getConnection();
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="\bootstrap\jquery-2.1.1.min.js"></script>
    <%-- <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script> --%>
    <script src="\bootstrap\js\bootstrap.js"></script>
    <link href="\bootstrap\css\bootstrap.css" rel="stylesheet" type="text/css">
    <title>Личный Кабинет</title>
</head>
<body style="background-color: #b0b0b0">
<div class="container">
    <%@include file="layouts/navigation.jsp" %>
    <div class="col-xs-12 col-sm-9">
        <div class="panel panel-info">
            <%
                boolean accessAdmin=false;
                if(request.getParameter("id")!=null) {
                    int reqId=Integer.parseInt(request.getParameter("id"));
                    if(CockID!=0&&admin&&reqId==CockID)
                    {
                      accessAdmin=true;
                    }


                try {
                    PreparedStatement statement = conn.prepareStatement("SELECT name,surname,email,login,date, photo from users where id=?");
                    statement.setInt(1,reqId);
                    ResultSet resultSet=statement.executeQuery();
                    resultSet.next();

                %>
            <div class="panel-heading">Личный Кабинет
                <%if(admin){%><a  class= "navbar-right" href="/admin">Администрирование</a>
        <%}%>
            </div>
            <div style="padding: 10px;">
                <div style="width: 30%;margin: 5px; display: inline-block;text-align: center"><img  class="img-responsive"
                                                                                src="<%=resultSet.getString("photo")%>"></div>
                <div style="display: inline-block;width: 65%;vertical-align: top" class="jumbotron">
                    <p>

                    <h2><%=resultSet.getString(1)+ " "+resultSet.getString(2)%></h2></p>
                    <p>Email: <%=resultSet.getString(3)%></p>

                    <p>Никнейм: <%=resultSet.getString(4)%></p>

                    <p>Дата регистрации: <%=resultSet.getString(5)%></p>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Мои рецензии
                        <a  class= "navbar-right" href="/addrev">Написать рецензию</a></div>
                </div>
            </div>
            <%} catch (SQLException e) {
                e.printStackTrace();
            }
            }%>

        </div>
    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <%@include file="layouts/rightBar.jsp" %>
    </div>
</div>
</body>
</html>
<%  conn.close();%>