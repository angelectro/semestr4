<%--
  Created by IntelliJ IDEA.
  User: ABC
  Date: 01.11.14
  Time: 4:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%
    request.setCharacterEncoding("UTF-8");
    InitialContext initContext = new InitialContext();
    DataSource ds = null;
    try {
        ds = (DataSource) initContext.lookup("java:comp/env/jdbc/postgres");
    } catch (NamingException e) {
        e.printStackTrace();
    }
    Connection conn = ds.getConnection();


%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="\bootstrap\jquery-2.1.1.min.js"></script>
    <%-- <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script> --%>
    <script src="\bootstrap\js\bootstrap.js"></script>
    <link href="\bootstrap\css\bootstrap.css" rel="stylesheet" type="text/css">
    <title>
    </title>
</head>
<body style="background-color: #b0b0b0">
<div class="container">
    <%@include file="layouts/navigation.jsp" %>
    <div class="col-xs-12 col-sm-9">
        <%
            boolean checkEmptyParam=false;
            boolean checkWrite=false;
            PreparedStatement statement=null;
            if (user) {
            if(request.getMethod().equals("POST")) {
                // statement = conn.prepareStatement("");

                if(request.getParameter("id").equals("")||request.getParameter("text").equals("")||request.getParameter("header").equals(""))
                {
                    checkEmptyParam=true;
                }
                else{
                    String reqId=request.getParameter("id");
                    String text=request.getParameter("text" );
                    String header =request.getParameter("header");
                    statement = conn.prepareStatement("insert into reviews (film_id,user_id,text,header) " +
                            "values(?,?,?,?)");
                    statement.setInt(1, Integer.parseInt(reqId));
                    statement.setInt(2, CockID);
                    statement.setString(3, text);
                    statement.setString(4,header);
                    statement.execute();
                    checkWrite=true;

                }
                 if(checkWrite){%>
        <div class="alert alert-info">Рецензия успешно добавлена
        <div><a href="addrev">Добавить рецензию</a> ещё</div></div>
        <%}

            }
        else{%>
        <div class="panel panel-info">
            <div class="panel-heading">Добавление рецензии
            </div>  <%if(checkEmptyParam){%>
            <div class="alert alert-danger">Все поля обязательны для заполнения!</div>
            <%}%>
            <div style="padding: 10px;">
                <form action="addrev" method="post">

                    <select class="form-control" name="id">
                        <option selected disabled>Выберите фильм</option>
                        <%
                            try {
                                statement = conn.prepareStatement("select id, name from films");
                                ResultSet resultSet = statement.executeQuery();
                                while (resultSet.next()) {
                        %>
                        <option value=<%=resultSet.getString(1)%>><%=resultSet.getString(2)%>
                        </option>
                        <%
                                }
                            } catch (SQLException e) {
                                System.err.println(e);
                            }
                        %>

                    </select>
                    <input class="form-control" type="text" placeholder="Введите постер..." name="header">
                    <input class="form-control" type="text" placeholder="Введите ваш текст..." name="text">
                    <input style="margin-top: 7px" class="btn btn-primary" type="submit" value="Добавить резензию"
                           width="15%">
                </form>
            </div>
        </div>
        <%}
        } else {%>
        <div class="alert alert-danger">Для добавления рецензии нужно <a href="/sigin">войти в профиль</a> или <a
                href="/signup">зарегистрироваться</a></div>
        <%}%>
    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <%@include file="layouts/rightBar.jsp" %>
    </div>
</div>
</body>
</html>
<%conn.close();%>
