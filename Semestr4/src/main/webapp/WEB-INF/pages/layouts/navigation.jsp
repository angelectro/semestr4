
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<nav style="margin-top: 20px" class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
        <div class="navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="${s:mvcUrl('HC#get').build()}">Главная</a></li>
                <li><a href="#">Расписание</a></li>
                <li><a href="${s:mvcUrl('NEWSC#get').build()}">Новинки</a></li>
                <li><a href="#">Рецензии</a></li>
                <li><a href="#">Как проехать</a></li>
            </ul>

            <form class="navbar-form navbar-right" role="sig_in">
                <div>
                    <div >
                        <span class="glyphicon glyphicon-user"></span>
                        <a href="/personal_cabinet?id=">Талипов Загит</a>
                    </div>
                    <div style="text-align-last: right">
                        <a  href="/log">Выйти</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</nav>
