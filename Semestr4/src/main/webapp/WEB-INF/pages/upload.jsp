<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="java.util.Random" %>
<%@ page import="java.nio.charset.Charset" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="org.apache.commons.fileupload.FileUploadException" %>
<%@ page import="java.io.PrintWriter" %>

<%
       Thread thread=new Thread(new Runnable() {
           @Override
           public void run() {
               boolean isMultipart = ServletFileUpload.isMultipartContent(request);
               if (!isMultipart) {
                   try {
                       response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                   } catch (IOException e) {
                       e.printStackTrace();
                   }
                   return;
               }

               // Создаём класс фабрику
               DiskFileItemFactory factory = new DiskFileItemFactory();

               // Максимальный буфера данных в байтах,
               // при его привышении данные начнут записываться на диск во временную директорию
               // устанавливаем один мегабайт
               factory.setSizeThreshold(1024 * 1024);

               // устанавливаем временную директорию
               File tempDir = (File) getServletConfig().getServletContext().getAttribute("javax.servlet.context.tempdir");
               factory.setRepository(tempDir);
               ServletFileUpload upload = new ServletFileUpload(factory);
               upload.setSizeMax(1024 * 1024 * 10);
               String pathS =null;
               try {
                   List items = upload.parseRequest(request);
                   Iterator iter = items.iterator();

                   while (iter.hasNext()) {
                       FileItem item = (FileItem) iter.next();

                       if (item.isFormField()) {
                           System.out.println(item.getFieldName() + "=" + item.getString());
                       } else {
                           File uploadetFile = null;
                           Random random = new Random();
                           do {
                               pathS = "/source/"+random.nextInt()+item.getName();
                               String path = getServletConfig().getServletContext().getRealPath(pathS);
                               uploadetFile = new File(path);
                           }
                           while (uploadetFile.exists());
                           uploadetFile.createNewFile();
                           item.write(uploadetFile);
                       }
                   }

                   getServletConfig().getServletContext().setAttribute("path", pathS);

               } catch (Exception e) {
                   e.printStackTrace();
                   try {
                       response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                   } catch (IOException e1) {
                       e1.printStackTrace();
                   }
                   return;
               }
           }
       });

    {
        response.setContentType("text/html;charset=UTF-8");

        try {

            DiskFileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            List items = upload.parseRequest(request);
            Iterator it = items.iterator();
            int i = 1;
            String fname = "";
            while (it.hasNext()) {
                FileItem object = (FileItem) it.next();
                if (object.isFormField()) {
                } else {
                    byte[] data = object.get();
                    Random random =new Random();
                    String pathS = "/source/"+random.nextInt()+object.getName();
                    fname= getServletConfig().getServletContext().getRealPath(pathS);
                    FileOutputStream fis = new FileOutputStream(fname);
                    fis.write(data);
                    fis.close();
                    getServletConfig().getServletContext().setAttribute("path", pathS);
                }
            }
            System.out.println(fname);
        } catch (FileUploadException ex) {
            ex.printStackTrace();
        } finally {
            response.sendRedirect("/admin");
        }
    }


%>
