<%-- Created by IntelliJ IDEA. --%>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.util.regex.Matcher" %>
<%@ page import="java.util.regex.Pattern" %>
<%
    request.setCharacterEncoding("UTF-8");
    InitialContext initContext = new InitialContext();
    DataSource ds = null;
    ds = (DataSource) initContext.lookup("java:comp/env/jdbc/postgres");
    boolean check = false;
    String exceptions[] = {"Не все поля заполнены!", "Пароли не совпадают!", "Слишком легкий пароль!" +
            "(пароль должен содержать как минимум 2-цифры, 2-буквы верхнего регистра)"};
    String exception = "";
    Connection conn = ds.getConnection();
    String name = "";
    String surname = null;
    String email = null;
    String login = "";
    String[] userData = new String[2];
    String password = "";
    String password2;
    String pathSource = "";
    if (request.getMethod().equals("POST"))
        if (ServletFileUpload.isMultipartContent(request)) {
            %>
<%@include file="upload.jsp"%>
<%

    } else {

        {
            name = request.getParameter("name");
            surname = request.getParameter("surname");
            email = request.getParameter("email");
            login = request.getParameter("login");
            password = request.getParameter("password");
            password2 = request.getParameter("password2");
            userData[0] = login;
            userData[1] = password;
            System.out.println(userData[1]);
            if (name.equals("") || surname.equals("") || email.equals("") || login.equals("") || password.equals("") || password2.equals("")) {
                exception = exceptions[0];

            } else {
                if (!password.equals(password2)) {
                    exception = exceptions[1];

                } else {

                    String patterns[] = {"(.*[0-9].*){2,}", "(.*[A-Z].*){2,}"};
                    for (String pat : patterns) {
                        Pattern pattern = Pattern.compile(pat);
                        Matcher matcher = pattern.matcher(password);
                        if (!matcher.find()) {

                            exception = exceptions[2];
                            break;
                        }
                    }
                }
            }

            if (exception.equals("")) {
                check = true;
%>
<%@include file="md5.jsp" %>
<%

                PreparedStatement statement = conn.prepareStatement("insert into users (name,surname,email,login,password, cookie) values " +
                        "(?,?,?,?,?,?)");
                statement.setString(1, name);
                statement.setString(2, surname);
                statement.setString(3, email);
                statement.setString(4, login);
                statement.setString(5, md5userData[1]);
                statement.setString(6, md5userData[0]);
                statement.execute();
                System.out.println(check);
            }
        }

    }%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="\bootstrap\jquery-2.1.1.min.js"></script>
    <%-- <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script> --%>
    <script src="\bootstrap\js\bootstrap.js"></script>
    <link href="\bootstrap\css\bootstrap.css" rel="stylesheet" type="text/css">
    <title>Регистрация</title>
</head>
<body style="background-color: #b0b0b0">
<div class="container">
    <%@include file="layouts/navigation.jsp" %>
    <div class="col-xs-12 col-sm-9">
        <div class="jumbotron" style="padding-left: 50px">
            <h2>Регистрация</h2>

            <%if (!check) {%>
            <div>
                <% if (request.getMethod().equals("POST") && !exception.equals("")) {%>
                <div class="alert alert-danger">

                    <%=exception%>
                </div>
                <%}%>


            </div>
            <div>
                <form class="navbar-form" action="admin" method="post" enctype="multipart/form-data" accept="image/*">
                    <input class="form-control" type="file" name="data" placeholder="фото">
                    <input style="margin-top: 7px" class="btn btn-primary" type="submit" value="Загрузить">
                </form>

                <form action="signup" class="navbar-form" method="post">
                    <input style="margin-top: 10px" class="form-control" type="text" placeholder="Имя" name="name"
                           value="<%=name!=null?name:""%>"> <br>
                    <input style="margin-top: 10px" class="form-control" type="text" placeholder="Фамилия"
                           value="<%=surname!=null?surname:""%>"
                           name="surname"> <br>
                    <input style="margin-top: 10px" class="form-control" type="email" placeholder="Email address"
                           value="<%=email!=null?email:""%>" name="email"> <br>
                    <input style="margin-top: 10px" class="form-control" type="text" placeholder="Логин" name="login"
                           value="<%=login!=null?login:""%>">
                    <br>
                    <input style="margin-top: 10px" class="form-control" type="password" placeholder="Пароль"
                           name="password"> <br>
                    <input style="margin-top: 10px" class="form-control" type="password" placeholder="Повторите пароль"
                           name="password2">
                    <br>
                    <input style="margin-top:10px; margin-right: 10px;" type="submit" class="btn btn-default"
                           value="Зарегистрироваться"> <br>
                </form>
            </div>
            <%} else {%>
            <div class="alert alert-info">
                Регистрация прошла успешно!
            </div>
            <%}%>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <%@include file="layouts/rightBar.jsp" %>
    </div>
</div>
</body>
</html>
<% conn.close();%>