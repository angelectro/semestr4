<%--
  Created by IntelliJ IDEA.
  User: ABC
  Date: 29.10.14
  Time: 22:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%
    int x=0;


    InitialContext initContext = new InitialContext();
    DataSource ds = null;
    try {
        ds = (DataSource) initContext.lookup("java:comp/env/jdbc/postgres");
    } catch (NamingException e) {
        e.printStackTrace();
    }
    Connection conn = ds.getConnection();

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="\bootstrap\jquery-2.1.1.min.js"></script>
    <%-- <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script> --%>
    <script src="\bootstrap\js\bootstrap.js"></script>
    <link href="\bootstrap\css\mycss.css" rel="stylesheet" type="text/css">
    <link href="\bootstrap\css\bootstrap.css" rel="stylesheet" type="text/css">

    <title>Расписание</title>
</head>
<body style="background-color: #b0b0b0">
<div class="container">
    <%@include file="layouts/navigation.jsp" %>
    <div class="col-xs-12 col-sm-9">
        <div class="jumbotron" style="padding-left: 50px">
            <h2>Расписание</h2>

            <div class="panel panel-default">
                <div class="panel-heading">Среда, 29 октября</div>
                <table class="table">
                    <colgroup width="35%"/>
                    <colgroup span="5" width="13%" align="center">

                    </colgroup>
                    <tr>
                        <td><a href="#" class="label label-info">18:00</a></td>
                        <td>etg</td>
                        <td>aevrg</td>
                        <td>aerg</td>
                        <td>etg</td>
                        <td>aevrg</td>
                    </tr>
                    <tr>
                        <td>ergf</td>
                        <td>erf</td>
                        <td>erfre</td>
                        <td>aerg</td>
                        <td>etg</td>
                        <td>aevrg</td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <%@include file="layouts/rightBar.jsp" %>
    </div>
</div>
</body>
</html>
<%conn.close();%>
