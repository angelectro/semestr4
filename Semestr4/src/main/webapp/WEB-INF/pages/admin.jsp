<%--
  Created by IntelliJ IDEA.
  User: ABC
  Date: 30.10.14
  Time: 13:40
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>


<%
    InitialContext initContext = new InitialContext();
    DataSource ds = null;
    try {
        ds = (DataSource) initContext.lookup("java:comp/env/jdbc/postgres");
    } catch (NamingException e) {
        e.printStackTrace();
    }
    Connection conn = ds.getConnection();

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="\bootstrap\jquery-2.1.1.min.js"></script>
    <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <script src="\bootstrap\js\bootstrap.js"></script>
    <link href="\bootstrap\css\bootstrap.css" rel="stylesheet" type="text/css">
    <title>ADMIN</title>
</head>
<body style="background-color: #b0b0b0">
<div class="container">
    <%@include file="layouts/navigation.jsp" %>
    <div class="col-xs-12 col-sm-9">

        <%
            if (!admin)
                response.sendRedirect("/home");
            request.setCharacterEncoding("UTF-8");
            boolean flag = false;
            ServletContext context = getServletConfig().getServletContext();
            System.out.println(request.getMethod());
            if (request.getMethod().equals("POST")) {
                if (!ServletFileUpload.isMultipartContent(request)) {
                    if (request.getParameter("tip").equals("1")) {
                        String name = request.getParameter("name");
                        String year = request.getParameter("year");
                        String producer = request.getParameter("producer");
                        String actors = request.getParameter("actors");
                        String length = request.getParameter("length");
                        String text = request.getParameter("text");
                        try {
                            if (!name.equals("") || !year.equals("") || !producer.equals("") || !actors.equals("") || !length.equals("") || !text.equals("")) {
                                PreparedStatement statement = conn.prepareStatement("insert into films (name,year,producer,actors,length,text,img)" +
                                        "values (?,?,?,?,?,?,?)");

                                statement.setString(1, name);
                                statement.setInt(2, Integer.parseInt(year));
                                statement.setString(3, producer);
                                statement.setString(4, actors);
                                statement.setInt(5, Integer.parseInt(length));
                                statement.setString(6, text);
                                statement.setString(7, context.getAttribute("path") != null ? (String) context.getAttribute("path") : "");
                                statement.execute();
                                flag = true;
                            }
                        } catch (SQLException e) {
                            System.err.println(e);
                        }
                    } else {
                        String id = request.getParameter("id");
                        try {
                            if (!id.equals("")) {
                                PreparedStatement statement = conn.prepareStatement("insert into slider (img_src,film_id)" +
                                        "values (?,?)");
                                System.out.println(id);
                                statement.setString(1, context.getAttribute("path") != null ? (String) context.getAttribute("path") : "");
                                statement.setInt(2, Integer.parseInt(id));
                                statement.execute();
                                flag = true;
                            }
                        } catch (SQLException e) {
                            System.err.println(e);
                        }
                    }
                } else {
                    System.out.println("Multi");
                    context.setAttribute("this", this);
                    System.out.println(this);%>
        <%@include file="upload.jsp" %>
        <%


                }
            }
            // pathSource = pathSource.replace("/", "\\");


        %>
        <div class="panel panel-default">
            <div class="panel-heading">Добавление фильмов</div>
            <div style="padding: 10px">
                <form class="navbar-form" action="admin" method="post" enctype="multipart/form-data">
                    <input class="form-control" type="file" name="data">
                    <input style="margin-top: 7px" class="btn btn-primary" type="submit" value="Загрузить">
                </form>

                <form action="admin" method="post">
                    <input class="form-control" type="text" placeholder="Название Фильма" name="name">
                    <input class="form-control" type="number" placeholder="Год выпуска" name="year">
                    <input class="form-control" type="text" placeholder="Режиссер" name="producer">
                    <input class="form-control" type="text" placeholder="В ролях" name="actors">
                    <input class="form-control" type="number" placeholder="Продолжительность фильма(в минутах)"
                           height="100px" name="length">
                    <input class="form-control" type="text" placeholder="Краткое описание" height="50px" name="text">
                    <input type="text" style="display: none" value="1" name="tip">
                    <input style="margin-top: 7px" class="btn btn-primary" type="submit" value="Добавить фильм"
                           width="15%">
                </form>
                <% if (!request.getMethod().equals("POST")) {%>
                <div class="alert alert-info">Все поля обязательны для заполнения</div>
                <%
                } else {
                    if (!flag) {
                %>
                <div class="alert alert-danger">Не все поля заполнены!</div>
                <%} else {%>
                <div class="alert alert-info">Фильм добавлен!</div>
                <%
                        }
                    }
                %>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Добавление сеансов</div>
            <form class="navbar-form">
                <p>
                    <select class="form-control" name="film">
                        <option selected disabled>Выберите фильм</option>
                        <%
                            try {
                                PreparedStatement statement = conn.prepareStatement("select id, name from films");
                                ResultSet resultSet = statement.executeQuery();
                                while (resultSet.next()) {
                        %>
                        <option value=<%=resultSet.getString(1)%>><%=resultSet.getString(2)%>
                        </option>
                        <%
                                }
                            } catch (SQLException e) {
                                System.err.println(e);
                            }
                        %>

                    </select>
                <p><input class="form-control" type="date" placeholder="Выберите дату показа" name="date"></p>
                </p>
                <p><input class="form-control" placeholder="Введите время показа(пример: 18:25; 20:00;...)" name="time">
                </p>
                <input class="btn btn-primary" type="submit" value="Добавить сеанс">

            </form>

        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Добавление слайдов</div>

            <div style="display: inline-block">
                <form class="navbar-form" action="admin" method="post" enctype="multipart/form-data">
                    <input class="form-control" type="file" name="data">
                    <input style="margin-top: 7px" class="btn btn-primary" type="submit" value="Загрузить">
                </form>
                <form action="admin" class="navbar-form" method="post">
                    <p>
                        <select class="form-control" name="id">
                            <option selected disabled>Выберите фильм</option>
                            <%
                                try {
                                    PreparedStatement statement = conn.prepareStatement("select id, name from films");
                                    ResultSet resultSet = statement.executeQuery();
                                    while (resultSet.next()) {
                            %>
                            <option value=<%=resultSet.getString(1)%>><%=resultSet.getString(2)%>
                            </option>
                            <%
                                    }
                                } catch (SQLException e) {
                                    System.err.println(e);
                                }
                            %>

                        </select>
                        <br>
                        <input type="text" style="display: none" value="2" name="tip">
                        <input style="margin-top: 7px" class="btn btn-primary" type="submit" value="Добавить слайдер">
                </form>
            </div>
            <%
                if (context.getAttribute("path") != null) {
            %>
            <div style="width: 40%;display: inline-block; padding: 20px; ">
                <img style="text-align: center" class="img-responsive" src="<%=context.getAttribute("path")%>">
            </div>
            <% getServletConfig().getServletContext().removeAttribute("path");
                }%>
        </div>

    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <%@include file="layouts/rightBar.jsp" %>
    </div>
</div>
</body>
</html>
<%
    conn.close();%>

