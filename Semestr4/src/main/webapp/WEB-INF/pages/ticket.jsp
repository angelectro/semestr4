<%--
  Created by IntelliJ IDEA.
  User: ABC
  Date: 31.10.14
  Time: 18:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%
    InitialContext initContext = new InitialContext();
    DataSource ds = null;
    try {
        ds = (DataSource) initContext.lookup("java:comp/env/jdbc/postgres");
    } catch (NamingException e) {
        e.printStackTrace();
    }
    Connection conn = ds.getConnection();
    conn.close();
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="\bootstrap\jquery-2.1.1.min.js"></script>
    <%-- <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script> --%>
    <script src="\bootstrap\js\bootstrap.js"></script>
    <link href="\bootstrap\css\bootstrap.css" rel="stylesheet" type="text/css">
    <link href="bootstrap\css\mycss.css">
    <title>Покупка билетов</title>
</head>
<body style="background-color: #b0b0b0">
<div class="container">
    <%@include file="layouts/navigation.jsp" %>
    <div class="col-xs-12 col-sm-9">

        <div class="panel panel-info">
            <div class="panel-heading">Покупка билетов</div>
            <div style="padding: 10px;">
                <div style="width: 30%;margin: 5px; display: inline-block">
                    <div class="list-group">
                        <p class="list-group-item ">Фильм<br></p>

                        <p class="list-group-item">Время<br></p>

                        <p class="list-group-item">Места<br></p>

                        <p class="list-group-item">Сумма<br></p> <br>

                    </div>
                </div>
                <div style="display: inline-block;width: 65%;vertical-align: top" class="jumbotron">
                    <table>
                        <%for (int i = 1; i <= 20; i++) {%>
                        <tr>
                            <td style="margin-right: 10px"><%=i%>
                            </td>
                            <%for (int j = 1; j <= 20; j++) {%>
                            <td><a href="#">
                                <div style="margin: 1px;border-radius: 4px; width: 15px; height: 15px; background-color: #245269"></div>
                            </a></td>
                            <%}%>
                            <td style="margin-left: 10px"><%=i%>
                            </td>
                        </tr>
                        <%}%>

                    </table>
                </div>
            </div>

        </div>
    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <%@include file="layouts/rightBar.jsp" %>
    </div>
</div>
</body>
</html>
