<%--
  Created by IntelliJ IDEA.
  User: ABC
  Date: 31.10.14
  Time: 17:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%
    InitialContext initContext = new InitialContext();
    DataSource ds = null;
    try {
        ds = (DataSource) initContext.lookup("java:comp/env/jdbc/postgres");
    } catch (NamingException e) {
        e.printStackTrace();
    }
    Connection conn = ds.getConnection();

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="\bootstrap\jquery-2.1.1.min.js"></script>
    <%-- <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script> --%>
    <script src="\bootstrap\js\bootstrap.js"></script>
    <link href="\bootstrap\css\bootstrap.css" rel="stylesheet" type="text/css">
    <% PreparedStatement statement = conn.prepareStatement("select * from films where id=?");
        statement.setInt(1, Integer.parseInt(request.getParameter("id")));
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        String name = resultSet.getString("name");%>
    <title><%=name%>
    </title>
</head>
<body style="background-color: #b0b0b0">
<div class="container">
    <%@include file="layouts/navigation.jsp" %>
    <div class="col-xs-12 col-sm-9">

        <div class="panel panel-info">
            <div class="panel-heading"><h4><%=name%>
            </h4></div>
            <div style="padding: 10px;">
                <div style="width: 30%;margin: 5px; display: inline-block"><img class="img-responsive"
                                                                                src="<%=resultSet.getString("img")%>">
                </div>
                <div style="display: inline-block;width: 65%;vertical-align: top" class="alert alert-success">
                    <p>

                    <p>Год выпуска: <%=resultSet.getString("year")%>
                    </p>

                    <p>Режиссер: <%=resultSet.getString("producer")%>
                    </p>

                    <p>В ролях: <%=resultSet.getString("actors")%>
                    </p>

                    <p>Продолжительность: <%=resultSet.getString("length")%> мин</p>
                </div>
                <div> Описание: <br>
                    <%=resultSet.getString("text")%>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Рецензии на фильм
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <%@include file="layouts/rightBar.jsp" %>
    </div>
</div>
</body>
</html>
<%conn.close();%>