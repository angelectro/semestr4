<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ABC
  Date: 31.03.2015
  Time: 19:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Log</title>
</head>
<body>

<table border="1" cellpadding="5">
    <tr>
        <th>time</th>
        <th>methodName</th>
        <th>result</th>
    </tr>
<c:forEach var="log" items="${listLog}" varStatus="status">
    <tr>
        <td>${log.time}</td>
        <td>${log.methodName}</td>
        <td>${log.result}</td>
    </tr>
</c:forEach>
</table>
</body>
</html>
