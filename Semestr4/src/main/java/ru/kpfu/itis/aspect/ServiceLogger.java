package ru.kpfu.itis.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kpfu.itis.dao.LogDao;
import ru.kpfu.itis.model.Log;

/**
 * Created by ABC on 30.03.2015.
 */

@Aspect
public class ServiceLogger {

    @Autowired
    private LogDao logDao;

   //@Pointcut("execution(* ru.kpfu.itis.dao..*(..))&&!within(ru.kpfu.itis.dao.impl.LogDAOImpl)&& !within(ru.kpfu.itis.dao.LogDao)")
   @Pointcut("@within(ru.kpfu.itis.Loggable)")
    public void loggableMethod() { }

    @Around("loggableMethod()")
    public Object log(ProceedingJoinPoint point) throws Throwable {
        String s=point.getSignature().toString();
        System.out.println(s);
        Object o=point.proceed();
        logDao.add(new Log(s,o.toString()));
        return o;
    }
}
