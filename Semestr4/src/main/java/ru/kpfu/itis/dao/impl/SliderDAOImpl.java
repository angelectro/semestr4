package ru.kpfu.itis.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.Loggable;
import ru.kpfu.itis.dao.SliderDAO;
import ru.kpfu.itis.model.Slider;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by ABC on 30.03.2015.
 */
@Repository
@Loggable
public class SliderDAOImpl implements SliderDAO {

    @Autowired
    public SliderDAOImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private JdbcTemplate jdbcTemplate;


    @Override
    public void saveOrUpdate(Slider slider) {

    }

    @Override
    public void delete(int id) {

    }

    @Override
    public Slider get(int id) {
        return null;
    }

    @Override
    public List<Slider> getAll() {
        String sql = "select * from slider";

        List<Slider> sliders = jdbcTemplate.query(sql, new RowMapper<Slider>() {
            @Override
            public Slider mapRow(ResultSet resultSet, int i) throws SQLException {
                Slider slider = new Slider();
                if (i == 0) {
                    slider.setFirst(true);
                }
                slider.setId(resultSet.getInt("id"));
                slider.setImg_src(resultSet.getString("img_src"));
                slider.setFilm_id(resultSet.getInt("film_id"));
                return slider;
            }
        });
        return sliders;
    }
}
