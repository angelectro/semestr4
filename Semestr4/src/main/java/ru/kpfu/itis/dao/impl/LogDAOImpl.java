package ru.kpfu.itis.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.dao.LogDao;
import ru.kpfu.itis.model.Log;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by ABC on 30.03.2015.
 */
@Repository
public class LogDAOImpl implements LogDao {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public LogDAOImpl(DataSource source) {
        jdbcTemplate = new JdbcTemplate(source);
    }

    @Override
    public void add(Log log) {
        String sql = "insert into log (methodname,result) values(?,?)";
        jdbcTemplate.update(sql,log.getMethodName(),log.getResult());
    }

    @Override
    public List<Log> getAll() {
        String sql="select * from log ";
        List<Log> logList=jdbcTemplate.query(sql, new RowMapper<Log>() {
            @Override
            public Log mapRow(ResultSet resultSet, int i) throws SQLException {
                Log log= new Log();
                log.setTime(resultSet.getTimestamp("time"));
                log.setMethodName(resultSet.getString("methodname"));
                log.setResult(resultSet.getString("result"));
                return log;
            }
        });
        return logList;
    }
}
