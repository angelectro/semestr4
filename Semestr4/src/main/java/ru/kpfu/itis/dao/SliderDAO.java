package ru.kpfu.itis.dao;

import ru.kpfu.itis.model.Slider;

import java.util.List;

/**
 * Created by ABC on 30.03.2015.
 */
public interface SliderDAO {
    public void saveOrUpdate(Slider slider);
    public void delete(int id);
    public Slider get(int id);
    public List<Slider> getAll();

}