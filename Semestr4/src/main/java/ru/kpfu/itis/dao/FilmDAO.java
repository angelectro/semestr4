package ru.kpfu.itis.dao;

import ru.kpfu.itis.model.Film;
import ru.kpfu.itis.model.Slider;

import java.util.List;

/**
 * Created by ABC on 30.03.2015.
 */
public interface FilmDAO {
    public void saveOrUpdate(Film film);
    public void delete(int id);
    public Film get(int id);
    public List<Film> getAll();
}
