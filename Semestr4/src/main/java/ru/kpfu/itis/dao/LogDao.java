package ru.kpfu.itis.dao;

import ru.kpfu.itis.model.Log;

import java.util.List;

/**
 * Created by ABC on 30.03.2015.
 */
public interface LogDao{
    public void add(Log log);
    public List<Log> getAll();

}
