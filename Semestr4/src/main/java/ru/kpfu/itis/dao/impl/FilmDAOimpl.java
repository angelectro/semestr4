package ru.kpfu.itis.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.Loggable;
import ru.kpfu.itis.dao.FilmDAO;
import ru.kpfu.itis.model.Film;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by ABC on 30.03.2015.
 */
@Repository
@Loggable
public class FilmDAOImpl implements FilmDAO {

    public FilmDAOImpl() {

    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public FilmDAOImpl(DataSource source) {
        jdbcTemplate = new JdbcTemplate(source);
    }

    @Override
    public void saveOrUpdate(Film film) {

    }

    @Override
    public void delete(int id) {

    }

    @Override
    public Film get(int id) {
        return null;
    }

    @Override
    public List<Film> getAll() {
        String sql = "select * from films";
        List<Film> films = jdbcTemplate.query(sql, new RowMapper<Film>() {
            @Override
            public Film mapRow(ResultSet resultSet, int i) throws SQLException {
                Film film = new Film();
                film.setId(resultSet.getInt("id"));
                film.setName(resultSet.getString("name"));
                film.setYear(resultSet.getString("year"));
                film.setProducer(resultSet.getString("producer"));
                film.setActors(resultSet.getString("actors"));
                film.setLength(resultSet.getInt("length"));
                film.setText(resultSet.getString("text"));
                film.setImg_source(resultSet.getString("img"));

                return film;
            }
        });
        return films;
    }
}
