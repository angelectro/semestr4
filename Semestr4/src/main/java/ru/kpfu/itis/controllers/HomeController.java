package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kpfu.itis.dao.SliderDAO;
import ru.kpfu.itis.model.Slider;

import java.util.List;

/**
 * Created by ABC on 22.03.2015.
 */
@Controller
@RequestMapping("/")
public class HomeController {
    @Autowired
    private SliderDAO sliderDAO;

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public ModelAndView get(ModelAndView view) {
        List<Slider> sliders = sliderDAO.getAll();
        view.addObject("listSlider", sliders);
        view.setViewName("home");
        return view;
    }
}
