package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kpfu.itis.dao.FilmDAO;

/**
 * Created by ABC on 30.03.2015.
 */
@Controller
@RequestMapping("/news")
public class NEWSController {
    @Autowired
    private FilmDAO filmDAO;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get(ModelAndView view) {
        view.addObject("films",filmDAO.getAll());
        view.setViewName("new");
        return view;
    }
}
