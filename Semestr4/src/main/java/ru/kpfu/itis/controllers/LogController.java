package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kpfu.itis.dao.LogDao;

/**
 * Created by ABC on 31.03.2015.
 */
@Controller
@RequestMapping("/log")
public class LogController {
    @Autowired
    LogDao logDao;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get(ModelAndView view) {
        view.addObject("listLog", logDao.getAll());
        view.setViewName("log");
        return view;
    }
}
