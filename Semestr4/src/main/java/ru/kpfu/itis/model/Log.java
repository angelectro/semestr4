package ru.kpfu.itis.model;

import java.sql.Timestamp;

/**
 * Created by ABC on 30.03.2015.
 */
public class Log {
    public Log(String methodName, String result) {
        this.methodName = methodName;
        this.result = result;
    }

    public Log() {
    }

    private Timestamp time;
    private String methodName;
    private String result;


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }


    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
