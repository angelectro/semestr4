package ru.kpfu.itis.model;

/**
 * Created by ABC on 30.03.2015.
 */
public class Slider {
    private int id;
    private boolean first;
public Slider(){

}
    public Slider(int film_id, int id, String img_src) {
        this.film_id = film_id;
        this.id = id;
        this.img_src = img_src;
    }

    private String img_src;
    private int film_id;

    public int getFilm_id() {
        return film_id;
    }

    public void setFilm_id(int film_id) {
        this.film_id = film_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg_src() {
        return img_src;
    }

    public void setImg_src(String img_src) {
        this.img_src = img_src;
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }
}
