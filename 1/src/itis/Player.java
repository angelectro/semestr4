package itis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by ABC on 14.02.2015.
 */
@Component
public class Player {
    @Autowired
    private Animal animal;

    public void play() {
        System.out.println(animal.name + " say " + animal.voice());
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }
}
