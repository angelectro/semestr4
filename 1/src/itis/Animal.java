package itis;

/**
 * Created by ABC on 14.02.2015.
 */
public abstract class Animal {
    public String name;
    public Animal(){}
    public Animal(String name)
    {
        this.name=name;
    }
    public abstract String voice();
}
