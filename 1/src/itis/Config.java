package itis;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ABC on 15.02.2015.
 */
@Configuration
@ComponentScan
public class Config {
    @Bean
    public Animal animal()
    {
        return new Kitten("Мура");
    }

}
