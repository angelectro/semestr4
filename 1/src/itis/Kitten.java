package itis;

import org.springframework.stereotype.Component;

/**
 * Created by ABC on 14.02.2015.
 */
@Component
public class Kitten extends Animal {

    public Kitten() {

    }

    public Kitten(String name) {
        super(name);
    }

    @Override
    public String voice() {
        return "мяу-мяу";
    }
}
