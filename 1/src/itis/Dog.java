package itis;

import org.springframework.stereotype.Component;

/**
 * Created by ABC on 14.02.2015.
 */
@Component
public class Dog extends Animal {
    public Dog()
    {
    }
    public Dog(String name) {
        super(name);
    }

    @Override
    public String voice() {
        return "хау-хау";
    }
}
